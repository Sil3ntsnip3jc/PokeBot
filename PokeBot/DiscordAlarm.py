#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import requests
from random import randint
from .Alarm import Alarm
from .utils import (get_args, reject_leftover_parameters, get_color,
                    get_static_map_url, get_image_url)

log = logging.getLogger('DiscordAlarm')
args = get_args()


class DiscordAlarm(Alarm):

    _defaults = {
        'pokemon': {
            'webhook_url': "",
            'username': "<pkmn>",
            'content': "",
            'icon_url': get_image_url(
                "monsters/<pkmn_id_3>_<form_id_or_empty>.png"),
            'avatar_url': get_image_url(
                "monsters/<pkmn_id_3>_<form_id_or_empty>.png"),
            'title': "A wild <pkmn> has appeared!",
            'url': "<gmaps>",
            'body': "Available until <24h_time> (<time_left>).",
            'color': "<iv>"
        },
        'egg': {
            'webhook_url': "",
            'username': "Egg",
            'content': "",
            'icon_url': get_image_url("eggs/<raid_level>.png"),
            'avatar_url': get_image_url("eggs/<raid_level>.png"),
            'title': "Raid is incoming!",
            'url': "<gmaps>",
            'body': (
                "A level <raid_level> raid will hatch <begin_24h_time> " +
                "(<begin_time_left>)."
            ),
            'color': "<raid_level>"
        },
        'raid': {
            'webhook_url': "",
            'username': "Raid",
            'content': "",
            'icon_url': get_image_url(
                "monsters/<pkmn_id_3>_<form_id_or_empty>.png"),
            'avatar_url': get_image_url("eggs/<raid_level>.png"),
            'title': "Level <raid_level> Raid is available against <pkmn>!",
            'url': "<gmaps>",
            'body': "The raid is available until <24h_time> (<time_left>).",
            'color': "<raid_level>"
        }
    }

    def __init__(self, settings, max_attempts):
        self.__max_attempts = max_attempts
        self.__avatar_url = settings.pop('avatar_url', "")
        self.__map = settings.pop('map', {})
        self.__pokemon = self.create_alert_settings(
            settings.pop('pokemon', {}), self._defaults['pokemon'])
        self.__egg = self.create_alert_settings(
            settings.pop('egg', {}), self._defaults['egg'])
        self.__raid = self.create_alert_settings(
            settings.pop('raid', {}), self._defaults['raid'])
        reject_leftover_parameters(settings, "'Alarm level in Discord alarm.")
        log.info("Discord Alarm has been created!")

    def create_alert_settings(self, settings, default):
        alert = {
            'webhook_url': settings.pop('webhook_url', default['webhook_url']),
            'username': settings.pop('username', default['username']),
            'avatar_url': settings.pop('avatar_url', default['avatar_url']),
            'content': settings.pop('content', default['content']),
            'icon_url': settings.pop('icon_url', default['icon_url']),
            'title': settings.pop('title', default['title']),
            'url': settings.pop('url', default['url']),
            'body': settings.pop('body', default['body']),
            'color': default['color'],
            'map': get_static_map_url(
                settings.pop('map', self.__map), args.gmaps_keys[randint(
                    0, len(args.gmaps_keys) - 1)])
        }
        reject_leftover_parameters(settings, "'Alert level in Discord alarm.")
        return alert

    def send_alert(self, alert, info):
        payload = {
            'username': self.replace(alert['username'], info)[:32],
            'content': self.replace(alert['content'], info),
            'avatar_url':  self.replace(alert['avatar_url'], info),
            'embeds': [{
                'title': self.replace(alert['title'], info),
                'url': self.replace(alert['url'], info),
                'description': self.replace(alert['body'], info),
                'color': get_color(self.replace(alert['color'], info)),
                'thumbnail': {'url': self.replace(alert['icon_url'], info)}
            }]
        }
        if alert['map'] is not None:
            payload['embeds'][0]['image'] = {
                'url': self.replace(alert['map'], {
                    'lat': info['lat'],
                    'lng': info['lng']
                })
            }
        args = {
            'url': alert['webhook_url'],
            'payload': payload
        }
        self.try_sending(log, "Discord", self.send_webhook, args,
                         self.__max_attempts)

    def pokemon_alert(self, pokemon_info):
        self.send_alert(self.__pokemon, pokemon_info)

    def raid_egg_alert(self, raid_info):
        self.send_alert(self.__egg, raid_info)

    def raid_alert(self, raid_info):
        self.send_alert(self.__raid, raid_info)

    def send_webhook(self, url, payload):
        resp = requests.post(url, json=payload, timeout=5)
        if resp.ok is True:
            log.info("Notification successful (returned {})".format(
                resp.status_code))
        else:
            log.info("Discord response was {}".format(resp.content))
            raise requests.exceptions.RequestException(
                "Response received {}, webhook not accepted.".format(
                    resp.status_code))
